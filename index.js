var request = require('request')
var google = require('googleapis')
var fs = require('fs')

let threads = require('./messages.json')
let newThreads = {}
let accessToken = null
const timeout = 1500 // ms
const developLoggin = false

console.log("Checking ChefMe API every " + timeout + " ms")
developLoggin ? console.log("Development log-output enabled") : console.log("Development log-output disabled")

// Load up initial promises
var promises = [
    accesTokenPromise = getAccessToken()
]

// Start service once we have accestoken and first set of threads
Promise.all(promises).then(values => {
    accessToken = values[0] // Assign accessToken to global scope
    startMessageService()
})

/**
 * Starts polling ChefMe API for new messages
 */
function startMessageService() {
    developLoggin ? console.log('checking for new messages...') : null
    getThreads().then(function (newThreads) {
        for(var key in newThreads){
            developLoggin ? console.log('checking thread id: ' + key) : null
            let data = {
                threadId: key,
                threadToken: newThreads[key].token,
                threadTopic: newThreads[key].menu_title,
                lastMessage: newThreads[key].last_message,
                lastMessageFrom: newThreads[key].last_message_by,
                date: newThreads[key].last_message_date,
                notificationStart: "true"
            }
            data.userId = newThreads[key].relations[data.lastMessageFrom].id
            data.image = newThreads[key].relations[data.lastMessageFrom].image
            data.firstName = newThreads[key].relations[data.lastMessageFrom].firstname
            
            if(threads.hasOwnProperty(key)) {
                if(threads[key].last_message_date < newThreads[key].last_message_date && newThreads[key].last_message_by != "666"){
                    pushNotification('new-message', 'Ny besked vedr. '+ newThreads[key].menu_title, newThreads[key].last_message, data)
                        .then(function (response) {
                            console.log('push notification sent!')
                        }
                    )
                }else if (threads[key].last_message_date < newThreads[key].last_message_date){
                    sendData('new-message', 'Ny besked vedr. '+ newThreads[key].menu_title, newThreads[key].last_message, data)
                        .then(function (response) {
                                console.log('data notification sent!')
                            }
                        )
                }
                else{
                        developLoggin ? console.log('no new messages') : null
                    }
            }
            else{
                developLoggin ? console.log('new thread detected!') : null
                pushNotification('new-message', 'Ny besked vedr. '+ newThreads[key].menu_title, newThreads[key].last_message, data)
                    .then(function (response) {
                            console.log('push notification sent!')
                        }
                    )
            }
        }
        threads = newThreads
        fs.writeFile('messages.json', JSON.stringify(threads, null, 2),function(err){
            if(err) throw err
        })
        setTimeout(function () {
            startMessageService()
        }, timeout)
    })
}

/**
 * Sends a push notification to a topic
 * @returns {Promise}
 */
function pushNotification(topic, title, body, data) {
    developLoggin ? console.log('Sending push notificaion...', accessToken) : null
    return new Promise(function (resolve, reject) {
        request.post("https://fcm.googleapis.com/v1/projects/chefme-ba60b/messages:send",
            {
                headers: {
                    'User-Agent': 'user-agen',
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-type': 'application/json'
                },
                body: JSON.stringify({
                    "message": {
                        "topic": topic,
                        "android": {
                            "notification": {
                                "body": body,
                                "title": title,
                                "click_action": ".inbox.ConversationActivity",
                            },
                            "data" : data
                        },
                        "data" : data
                    }
                })
            }, function (err, res, data) {
                if (err) {
                    console.log('Error:', err)
                    reject(err)
                }
                else if (res.statusCode !== 200) {
                    console.log('Status:', res.statusCode, res)
                    reject(res)
                }
                else {
                    resolve(data)
                }
            })
    })
}

function sendData(topic, title, body, data) {
    developLoggin ? console.log('Sending data notificaion...', accessToken) : null
    return new Promise(function (resolve, reject) {
        request.post("https://fcm.googleapis.com/v1/projects/chefme-ba60b/messages:send",
            {
                headers: {
                    'User-Agent': 'user-agen',
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-type': 'application/json'
                },
                body: JSON.stringify({
                    "message": {
                        "topic": topic,
                        "android": {
                            "data" : data
                        },
                        "data" : data
                    }
                })
            }, function (err, res, data) {
                if (err) {
                    console.log('Error:', err)
                    reject(err)
                }
                else if (res.statusCode !== 200) {
                    console.log('Status:', res.statusCode, res)
                    reject(res)
                }
                else {
                    resolve(data)
                }
            })
    })
}
/**
 * Get all threads for hardcoded user
 * @returns {Promise}
 */
function getThreads() {
    return new Promise(
        function (resolve, reject) {
            request.get({
                url: 'https://chefme.dk/api/gcK3ULuZ8aCIdjP-50XoWlyVbHf29GDYrJnw/threads/all',
                json: true,
                headers: {'User-Agent': 'request'}
            }, function (err, res, data) {
                if (err) {
                    console.log('Error:', err)
                    reject(res)
                }
                else if (res.statusCode !== 200) {
                    console.log('Status:', res.statusCode)
                    reject(res)
                }
                else {
                    response = {}
                    data.result.forEach(thread => {
                        response[thread.thread_id] = thread
                    })
                    resolve(response)
                }
            })
        }
    )
}

/**
 * Gets google jwt access token through service-account.json settings-file
 * @returns {Promise}
 */
function getAccessToken() {
    return new Promise(function (resolve, reject) {
        var scopes = [
            "https://www.googleapis.com/auth/firebase.messaging"
        ]
        var key = require('./service-account.json')
        var jwtClient = new google.auth.JWT(
            key.client_email,
            null,
            key.private_key,
            scopes,
            null
        )
        jwtClient.authorize(function (err, tokens) {
            if (err) {
                reject(err)
                return
            }
            resolve(tokens.access_token)
        })
    })
}