# nodejs-server
This project has been created to bypass chefme's api sending push-notifications. We are polling for new messages periodically and checking timestamps in order to send notifications to devices upon recieving new messages.
## Requirements:
1. nodejs
2. npm

## Starting the server:
Download ```service-account.json``` from [google firebase console](https://console.firebase.google.com/u/0/project/_/settings/serviceaccounts/adminsdk) for the respective project.

Put the file in the root of this project (DO NOT stage it for git)

Install needed dependencies by running:

```npm install```

Start the server by running:

```npm start```